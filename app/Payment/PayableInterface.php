<?php

namespace App\Payment;

interface PayableInterface 
{
	public function pay();
}