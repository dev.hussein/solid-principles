<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    public function subscribe()
    {
    	// subscribe logic
    }

    public function unsubscribe()
    {
    	// unsubscribe logic
    }

    public function getNotifyEmail()
    {
    	// gets the email address for sending email notifications
    }
}
