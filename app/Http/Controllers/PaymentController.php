<?php

namespace App\Http\Controllers;

use App\Payment\PaymentFactory;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function pay(Request $request)
    {
    	$paymentFactory = new PaymentFactory();
    	$payment = $paymentFactory->initializePayment($request->type);
    	$payment->pay();
    }
}
