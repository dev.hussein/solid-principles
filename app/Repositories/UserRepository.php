<?php 

namespace App\Repositories;

use App\Interfaces\UserRepositoryInterface;
use App\User;
use Carbon\Carbon;

class UserRepository implements UserRepositoryInterface
{

	/**
	 * Gets user records from the database registered after certain date
	 * @param  Carbon $date 
	 * @return Illuminate\Database\Eloquent\Collection
	 */
	public function getAfterDate(Carbon $date)
	{	
		return User::where('created_at', '>', $date)->get();
	}

	/**
	 * Saves the resource in the database
	 * @param  object $data 
	 * @return App\User
	 */
	public function create($data)
	{
		$user = new User;
		$user->name = $data->name;
		$user->email = $data->email;
		$user->password = bcrypt($data->password);
		$user->save();

		return $user;
	}
}