<?php

namespace App\Services;

use App\Interfaces\NotifiableInterface;
use Illuminate\Support\Facades\Mail;

class Notifications
{
	public function send(NotifiableInterface $subscriber, $message)
	{
		Mail::to($subscriber->getNotifyEmail())
			->queue($message);
	}
}