<?php

namespace App\Interfaces;

interface NotifiableInterface
{
	public function getNotifyEmail() : string;
}