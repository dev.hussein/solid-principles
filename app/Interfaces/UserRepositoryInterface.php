<?php

namespace App\Interfaces;

use Carbon\Carbon;
use User;

interface UserRepositoryInterface
{
	public function getAfterDate(Carbon $date);

	public function create(User $user);
}